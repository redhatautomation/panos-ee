# panos-ee
This EE contains the paloaltonetworks.panos collection and python requirments as provided from https://galaxy.ansible.com/paloaltonetworks/panos

The panos-ee can be pulled without credentials from:
~~~
registry.gitlab.com/redhatautomation/panos-ee:latest
~~~

## ansible.cfg

The ansible.cfg file includes the following entry to ensure either the ansible-navigator or controller select the python interpetor containing the pan-os-python library.
~~~
interpreter_python=/usr/bin/python3
~~~

## Run a playbook or job-template

Run 1-facts.yml to test the EE. This playbook requires a PAN device.

~~~
ansible-navigator run 1-facts.yml -m stdout

Example Output:
---------------------------------------------------------------------------------------
Execution environment image and pull policy overview
---------------------------------------------------------------------------------------
Execution environment image name:     registry.gitlab.com/redhatautomation/panos-ee:latest
Execution environment image tag:      latest
Execution environment pull arguments: None
Execution environment pull policy:    tag
Execution environment pull needed:    True
---------------------------------------------------------------------------------------
Updating the execution environment
---------------------------------------------------------------------------------------
Running the command: docker pull registry.gitlab.com/redhatautomation/panos-ee:latest
latest: Pulling from redhatautomation/panos-ee
Digest: sha256:7cbdf334528d52317bb34b791c20e44b475d9a4388acaf5e36b295426b0fd87c
Status: Image is up to date for registry.gitlab.com/redhatautomation/panos-ee:latest
registry.gitlab.com/redhatautomation/panos-ee:latest

PLAY [Retrieve data about objects from the firewall] ***************************

TASK [check if ready] **********************************************************
changed: [localhost]

TASK [Get the system info] *****************************************************
changed: [localhost]

TASK [debug] *******************************************************************
ok: [localhost] => {
    "msg": {
        "response": {
            "@status": "success",
            "result": {
                "system": {
                    "app-version": "8462-6955",
                    "av-version": "0",
                    "cloud-mode": "cloud",
                    "default-gateway": "172.31.16.1",
                    "device-certificate-status": "None",
                    "device-dictionary-release-date": "2022/06/23 19:01:45 PDT",
                    "device-dictionary-version": "52-333",
                    "devicename": "PA-VM",
                    "family": "vm",
                    "global-protect-client-package-version": "0.0.0",
                    "global-protect-clientless-vpn-version": "0",
                    "global-protect-datafile-release-date": "unknown",
                    "global-protect-datafile-version": "unknown",
                    "hostname": "PA-VM",
                    "ip-address": "172.31.22.131",
                    "ipv6-address": "unknown",
                    "ipv6-link-local-address": "fe80::887:1cff:fee7:799d/64",
                    "is-dhcp": "yes",
                    "logdb-version": "10.0.3",
                    "mac-address": "0a:87:1c:e7:79:9d",
                    "model": "PA-VM",
                    "multi-vsys": "off",
                    "netmask": "255.255.240.0",
                    "operational-mode": "normal",
                    "platform-family": "vm",
                    "plugin_versions": {
                        "entry": [
                            {
                                "@name": "dlp",
                                "@version": "1.0.3",
                                "pkginfo": "dlp-1.0.3"
                            },
                            {
                                "@name": "vm_series",
                                "@version": "2.0.7",
                                "pkginfo": "vm_series-2.0.7"
                            }
                        ]
                    },
                    "public-ip-address": "unknown",
                    "serial": "407CD40BE1AEC3A",
                    "sw-version": "10.0.8-h8",
                    "threat-version": "8462-6955",
                    "time": "Tue Jul  5 12:27:24 2022",
                    "uptime": "0 days, 2:07:16",
                    "url-db": "paloaltonetworks",
                    "url-filtering-version": "0000.00.00.000",
                    "vm-cap-tier": "16.0 GB",
                    "vm-cores": "8",
                    "vm-cpuid": "AWSMP:03A53858D1F0D8105:e9yfvyj3uag5uo5j2hjikv74n:us-east-1",
                    "vm-license": "VM-500",
                    "vm-mem": "32467048",
                    "vm-mode": "Amazon AWS",
                    "vm-uuid": "EC2EB690-84A1-6CD9-A832-1744904C7BDC",
                    "vpn-disable-mode": "off",
                    "wf-private-release-date": "unknown",
                    "wf-private-version": "0",
                    "wildfire-rt": "Disabled",
                    "wildfire-version": "0"
                }
            }
        }
    }
}
~~~